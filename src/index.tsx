import EventEmitter from 'eventemitter3'
import React from 'react'
import {
  AppRegistry,
  ComponentProvider,
  EventSubscriptionVendor,
  NativeEventEmitter,
  NativeModules,
} from 'react-native'

const APP_DID_STARTED_EVENT = 'RNNL.appDidStarted'
const SCREEN_DID_APPEAR_EVENT = 'RNNL.screenDidAppear'
const SCREEN_DID_DISAPPEAR_EVENT = 'RNNL.screenDidDisappear'
const SCREEN_WILL_APPEAR_EVENT = 'RNNL.screenWillAppear'
const SCREEN_WILL_DISAPPEAR_EVENT = 'RNNL.screenWillDisappear'
const SCREEN_DID_REMOVED_EVENT = 'RNNL.screenDidRemoved'

type ILayout = IStack | IScreen
interface IStack {
  type: 'stack'
  navBar?: INativeNavBar
  children: IScreen[]
}
interface IScreen {
  type: 'screen'
  componentName: string
  id: string
  navigationTitle?: string
  props?: IScreenAdditionalProps
}

type INativeLayout = INativeStack | INativeScreen
interface INativeStack {
  type: 'stack'
  navBar: INativeNavBar
  children: INativeScreen[]
}
interface INativeScreen {
  type: 'screen'
  componentName: string
  id: string
  navigationTitle?: string
}
interface INativeNavBar {
  hidden?: boolean
}

export interface IScreenEvent {
  screenId: string
}

interface INativeNavigator extends EventSubscriptionVendor {
  present(targetId: string, layout: INativeLayout): Promise<void>
  dismiss(targetId: string): Promise<void>
  push(targetId: string, layout: INativeLayout): Promise<void>
  pop(targetId: string): Promise<void>
}

interface INavigationEventMap {
  screenWillAppear: IScreenEvent
  screenDidAppear: IScreenEvent
  screenWillDisappear: IScreenEvent
  screenDidDisappear: IScreenEvent
}

interface IScreenAdditionalProps {
  [k: string]: any
}
interface IScreenProps {
  id: string
}

class Navigator {
  private _nativeNavigator: INativeNavigator
  private _nativeEmitter: NativeEventEmitter
  private _emitter: EventEmitter = new EventEmitter()
  private _currentScreenId: string | undefined
  private _screenProps: Map<string, IScreenAdditionalProps> = new Map()

  constructor() {
    this._nativeNavigator = NativeModules.RNNLNavigator
    this._nativeEmitter = new NativeEventEmitter(this._nativeNavigator)
    this._nativeEmitter.addListener(SCREEN_DID_APPEAR_EVENT, this.screenDidAppear)
    this._nativeEmitter.addListener(SCREEN_DID_DISAPPEAR_EVENT, this.screenDidDisappear)
    this._nativeEmitter.addListener(SCREEN_WILL_APPEAR_EVENT, this.screenWillAppear)
    this._nativeEmitter.addListener(SCREEN_WILL_DISAPPEAR_EVENT, this.screenWillDisappear)
    this._nativeEmitter.addListener(SCREEN_DID_REMOVED_EVENT, this.screenDidRemoved)
  }

  get currentScreenId(): string | undefined {
    return this._currentScreenId
  }

  onAppStarted(cb: () => void): void {
    this._nativeEmitter.addListener(APP_DID_STARTED_EVENT, cb)
  }

  present(targetId: string, layout: ILayout): Promise<void> {
    const nativeLayout = this.processLayout(layout)
    return this._nativeNavigator.present(targetId, nativeLayout)
  }
  dismiss(targetId: string): Promise<void> {
    return this._nativeNavigator.dismiss(targetId)
  }
  pushScreen(layout: ILayout): Promise<void> {
    if (!this._currentScreenId) throw new Error('no current screen')
    const nativeLayout = this.processLayout(layout)
    return this._nativeNavigator.push(this._currentScreenId, nativeLayout)
  }
  popScreen(): Promise<void> {
    if (!this._currentScreenId) throw new Error('no current screen')
    return this._nativeNavigator.pop(this._currentScreenId)
  }

  on<K extends keyof INavigationEventMap>(event: K, listener: (ev: INavigationEventMap[K]) => void) {
    this._emitter.on(event, listener)
  }
  off<K extends keyof INavigationEventMap>(event: K, listener: (ev: INavigationEventMap[K]) => void) {
    this._emitter.off(event, listener)
  }
  once<K extends keyof INavigationEventMap>(event: K, listener: (ev: INavigationEventMap[K]) => void) {
    this._emitter.once(event, listener)
  }

  registerScreen(name: string, provider: ComponentProvider) {
    AppRegistry.registerComponent(name, this.wrapScreenProvider(provider))
  }
  hasScreen(screenId: string) {
    return this._screenProps.has(screenId)
  }

  private processLayout(layout: ILayout): INativeLayout {
    switch (layout.type) {
      case 'screen':
        return this.processScreenLayout(layout)
      case 'stack':
        return this.processStackLayout(layout)
    }
  }

  private processScreenLayout(layout: IScreen): INativeScreen {
    this._screenProps.set(layout.id, layout.props || {})
    const nativeScreen: INativeScreen = {
      componentName: layout.componentName,
      id: layout.id,
      type: layout.type,
    }
    if (layout.navigationTitle) nativeScreen.navigationTitle = layout.navigationTitle
    return nativeScreen
  }

  private processStackLayout(layout: IStack): INativeStack {
    const navBar = layout.navBar ? layout.navBar : { hidden: true }
    return {
      children: layout.children.map((l) => this.processScreenLayout(l)),
      navBar,
      type: layout.type,
    }
  }

  private screenDidAppear = (ev: IScreenEvent) => {
    // tslint:disable-next-line:no-console
    console.log(`screen '${ev.screenId}' did appear`)
    this._currentScreenId = ev.screenId
    this._emitter.emit('screenDidAppear', ev)
  }
  private screenDidDisappear = (ev: IScreenEvent) => {
    // tslint:disable-next-line:no-console
    console.log(`screen '${ev.screenId}' did disappear`)
    this._emitter.emit('screenDidDisappear', ev)
  }
  private screenWillAppear = (ev: IScreenEvent) => {
    // tslint:disable-next-line:no-console
    console.log(`screen '${ev.screenId}' will appear`)
    this._currentScreenId = ev.screenId
    this._emitter.emit('screenWillAppear', ev)
  }
  private screenWillDisappear = (ev: IScreenEvent) => {
    // tslint:disable-next-line:no-console
    console.log(`screen '${ev.screenId}' will disappear`)
    this._emitter.emit('screenWillDisappear', ev)
  }
  private screenDidRemoved = (ev: IScreenEvent) => {
    // tslint:disable-next-line:no-console
    console.log(`screen '${ev.screenId}' did removed`)
    this._screenProps.delete(ev.screenId)
  }

  private wrapScreenProvider(provider: ComponentProvider): ComponentProvider {
    return () => {
      const Screen = provider()
      const NavWrapper: React.SFC<IScreenProps> = (props) => {
        const screenId = props.id
        const additionalProps = this._screenProps.get(screenId)
        return <Screen {...props} {...additionalProps} />
      }
      return NavWrapper
    }
  }
}

export default new Navigator()
