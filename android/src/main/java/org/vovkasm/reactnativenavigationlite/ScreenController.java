package org.vovkasm.reactnativenavigationlite;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.bluelinelabs.conductor.Controller;
import com.bluelinelabs.conductor.ControllerChangeHandler;
import com.bluelinelabs.conductor.ControllerChangeType;
import com.facebook.react.ReactApplication;
import com.facebook.react.ReactInstanceManager;
import com.facebook.react.ReactNativeHost;
import com.facebook.react.ReactRootView;
import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.ReactContext;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.modules.core.DeviceEventManagerModule;

import org.vovkasm.reactnativenavigationlite.layout.Screen;

public class ScreenController extends Controller {
    private static final String TAG = ScreenController.class.getSimpleName();

    private @Nullable
    ReactRootView mReactRootView;

    private String mScreenId;
    private String mComponentName;

    public ScreenController (Screen screen) {
        super();
        mScreenId = screen.getId();
        mComponentName = screen.getComponentName();
    }

    public ScreenController(@Nullable Bundle args) {
        super(args);
        if (args != null) {
            onRestoreInstanceState(args);
        }
    }


    @NonNull
    @Override
    protected View onCreateView(@NonNull LayoutInflater inflater, @NonNull ViewGroup container) {
        logWithInstance( "onCreateView");

        Bundle arguments = new Bundle();
        arguments.putString("componentName", mComponentName);
        arguments.putString("id", mScreenId);

        Activity activity = getActivity();
        if (activity == null) {
            throw new RuntimeException("Can't find activity to create react root view");
        }

        FrameLayout frameLayout = new FrameLayout(getActivity());
        mReactRootView = new ReactRootView(getActivity());
        mReactRootView.setLayoutParams(new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        mReactRootView.startReactApplication(getReactNativeHost().getReactInstanceManager(), mComponentName, arguments);
        frameLayout.addView(mReactRootView);
        return frameLayout;
    }

    @Override
    protected void onDestroyView(@NonNull View view) {
        super.onDestroyView(view);
        if (mReactRootView != null) {
            mReactRootView.unmountReactApplication();
            mReactRootView = null;
        }
        sendScreenEvent("RNNL.screenDidRemoved");
    }

    @Override
    protected void onChangeStarted(@NonNull ControllerChangeHandler changeHandler, @NonNull ControllerChangeType changeType) {
        super.onChangeStarted(changeHandler, changeType);
        if (changeType.isEnter) {
            sendScreenEvent("RNNL.screenWillAppear");
        } else {
            sendScreenEvent("RNNL.screenWillDisappear");
        }
    }

    @Override
    protected void onChangeEnded(@NonNull ControllerChangeHandler changeHandler, @NonNull ControllerChangeType changeType) {
        super.onChangeEnded(changeHandler, changeType);
        if (changeType.isEnter) {
            sendScreenEvent("RNNL.screenDidAppear");
        } else {
            sendScreenEvent("RNNL.screenDidDisappear");
        }
    }

    private void sendScreenEvent(String name) {
        if (getReactNativeHost().hasInstance()) {
            ReactInstanceManager instanceManager = getReactNativeHost().getReactInstanceManager();
            ReactContext context = instanceManager.getCurrentReactContext();
            WritableMap event = Arguments.createMap();
            event.putString("screenId", mScreenId);
            context.getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class).emit(name, event);

        } else {
            Log.e(TAG, "[" + mScreenId + "] Can't get react instance to send event " + name);
        }
    }

    private ReactNativeHost getReactNativeHost() {
        Activity activity = getActivity();
        if (activity == null) return null;
        return ((ReactApplication) activity.getApplication()).getReactNativeHost();
    }

    private void logWithInstance(String msg) {
        Log.d(TAG, "[" + mScreenId + "] " + msg);
    }

}
