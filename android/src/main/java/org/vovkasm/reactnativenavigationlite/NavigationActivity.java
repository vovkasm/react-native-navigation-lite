package org.vovkasm.reactnativenavigationlite;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.widget.FrameLayout;

import com.bluelinelabs.conductor.Conductor;
import com.bluelinelabs.conductor.Router;
import com.bluelinelabs.conductor.RouterTransaction;
import com.bluelinelabs.conductor.changehandler.HorizontalChangeHandler;
import com.facebook.react.modules.core.DefaultHardwareBackBtnHandler;

import org.vovkasm.reactnativenavigationlite.layout.Screen;
import org.vovkasm.reactnativenavigationlite.layout.Stack;

public class NavigationActivity extends AppCompatActivity implements DefaultHardwareBackBtnHandler, NavigatorModule.Delegate {
    private static final String TAG = NavigationActivity.class.getSimpleName();

    private Router mRouter;
    private FrameLayout mContent;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate");

        mContent = new FrameLayout(this);
        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT);
        mContent.setLayoutParams(layoutParams);

        setContentView(mContent);

        mRouter = Conductor.attachRouter(this, mContent, null);

        NavigationProvider.getInstance().onActivityCreate(this, mContent, null);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        Log.d(TAG, "onNewIntent");
        NavigationProvider.getInstance().onActivityNewIntent(intent);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "onResume");
        NavigationProvider.getInstance().onActivityResume(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(TAG, "onPause");
        NavigationProvider.getInstance().onActivityPause(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy");
        NavigationProvider.getInstance().onActivityDestroy(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d(TAG, "onActivityResult");
        NavigationProvider.getInstance().onActivityResult(this, requestCode, resultCode, data);
    }

    @Override
    public void onBackPressed() {
        NavigationProvider.getInstance().onActivityBackPressed(this);
    }

    @Override
    public void invokeDefaultOnBackPressed() {
        if (mRouter.handleBack()) {
            return;
        }
        super.onBackPressed();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        return NavigationProvider.getInstance().onKeyDown(keyCode, event) || super.onKeyDown(keyCode, event);
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        return NavigationProvider.getInstance().onKeyUp(keyCode, event) || super.onKeyUp(keyCode, event);
    }

    @Override
    public void present(Stack stack) {
        boolean rootDone = false;
        for (Screen screen : stack.getChildren()) {
            ScreenController screenController = new ScreenController(screen);
            if (rootDone) {
                mRouter.pushController(RouterTransaction.with(screenController));
            } else {
                mRouter.setRoot(RouterTransaction.with(screenController));
                rootDone = true;
            }
        }
    }

    @Override
    public void push(Screen screen) {
        ScreenController screenController = new ScreenController(screen);
        RouterTransaction transaction = RouterTransaction.with(screenController)
                .popChangeHandler(new HorizontalChangeHandler())
                .pushChangeHandler(new HorizontalChangeHandler());
        mRouter.pushController(transaction);
    }

    @Override
    public void pop() {
        mRouter.popCurrentController();
    }
}
