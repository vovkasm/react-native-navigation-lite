package org.vovkasm.reactnativenavigationlite;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.KeyEvent;
import android.view.ViewGroup;

import com.bluelinelabs.conductor.Conductor;
import com.bluelinelabs.conductor.Router;
import com.bluelinelabs.conductor.RouterTransaction;
import com.facebook.react.ReactInstanceManager;
import com.facebook.react.ReactNativeHost;
import com.facebook.react.bridge.ReactContext;
import com.facebook.react.devsupport.DoubleTapReloadRecognizer;
import com.facebook.react.modules.core.DeviceEventManagerModule;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class NavigationProvider {
    public static NavigationProvider getInstance() {
        if (sInstance == null) {
            synchronized (NavigationProvider.class) {
                if (sInstance == null) sInstance = new NavigationProvider();
            }
        }
        return sInstance;
    }

    public void setReactHost(ReactNativeHost reactHost) {
        mReactHost = reactHost;
    }

    public @Nullable ReactNativeHost getReactHost() {
        return mReactHost;
    }
    public @Nullable ReactInstanceManager getReactInstanceManager() {
        if (mReactHost == null) return null;
        return mReactHost.getReactInstanceManager();
    }
    public boolean hasReactInstanceManager() {
        return mReactHost != null && mReactHost.hasInstance();
    }

    /**
     * Activity tracking
     */

    public void onActivityCreate(final NavigationActivity activity, ViewGroup container, @Nullable Bundle savedInstanceState) {
        registerActivity(activity, container, savedInstanceState);
        if (mReactHost == null) return;
        if (mReactHost.getUseDeveloperSupport()) {
            mDoubleTapReloadRecognizer = new DoubleTapReloadRecognizer();
        }
        final ReactInstanceManager reactInstanceManager = getReactInstanceManager();
        if (reactInstanceManager == null) return;
        final ReactContext reactContext = reactInstanceManager.getCurrentReactContext();
        if (reactContext != null) {
            markReactContextInitialized(activity, reactContext);
        } else {
            reactInstanceManager.createReactContextInBackground();
            reactInstanceManager.addReactInstanceEventListener(
                    new ReactInstanceManager.ReactInstanceEventListener() {
                        @Override
                        public void onReactContextInitialized(ReactContext context) {
                            markReactContextInitialized(activity, context);
                        }
                    }
            );
        }
    }

    public void onActivityNewIntent(Intent intent) {
        final ReactInstanceManager reactInstanceManager = getReactInstanceManager();
        if (reactInstanceManager == null) return;
        reactInstanceManager.onNewIntent(intent);
    }

    public void onActivityResume(final NavigationActivity activity) {
        final ReactInstanceManager reactInstanceManager = getReactInstanceManager();
        if (reactInstanceManager == null) return;
        reactInstanceManager.onHostResume(activity, activity);
    }

    public void onActivityPause(final NavigationActivity activity) {
        if (!hasReactInstanceManager()) return;
        final ReactInstanceManager reactInstanceManager = getReactInstanceManager();
        if (reactInstanceManager == null) return;
        reactInstanceManager.onHostPause(activity);
    }

    public void onActivityDestroy(final NavigationActivity activity) {
        unregisterActivity(activity);
        if (!hasReactInstanceManager()) return;
        final ReactInstanceManager reactInstanceManager = getReactInstanceManager();
        if (reactInstanceManager == null) return;
        reactInstanceManager.onHostDestroy(activity);
        if (mDoubleTapReloadRecognizer != null) {
            mDoubleTapReloadRecognizer = null;
        }
    }

    public void onActivityResult(final NavigationActivity activity, int requestCode, int resultCode, Intent data) {
        final ReactInstanceManager reactInstanceManager = getReactInstanceManager();
        if (reactInstanceManager == null) return;
        reactInstanceManager.onActivityResult(activity, requestCode, resultCode, data);
    }

    public void onActivityBackPressed(final NavigationActivity activity) {
        final ReactInstanceManager reactInstanceManager = getReactInstanceManager();
        if (reactInstanceManager != null) {
            reactInstanceManager.onBackPressed();
        } else {
            activity.invokeDefaultOnBackPressed();
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (mReactHost != null && mReactHost.getUseDeveloperSupport()) {
            if (hasReactInstanceManager() && keyCode == KeyEvent.KEYCODE_MEDIA_FAST_FORWARD) {
                event.startTracking();
                return true;
            }
        }
        return false;
    }

    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (mReactHost != null && mReactHost.getUseDeveloperSupport()) {
            final ReactInstanceManager reactInstanceManager = getReactInstanceManager();
            if (reactInstanceManager != null) {
                if (keyCode == KeyEvent.KEYCODE_MENU) {
                    reactInstanceManager.showDevOptionsDialog();
                    return true;
                }
                if (mDoubleTapReloadRecognizer != null) {
                    boolean didDoubleTapR = mDoubleTapReloadRecognizer.didDoubleTapR(keyCode, null);
                    if (didDoubleTapR) {
                        reactInstanceManager.getDevSupportManager().handleReloadJS();
                        resetAllRouters();
                        return true;
                    }
                }
            }
        }
        return false;
    }

    private void registerActivity(final NavigationActivity activity, ViewGroup container, @Nullable Bundle savedInstanceState) {
        final Router router = Conductor.attachRouter(activity, container, savedInstanceState);
        mRouters.put(activity, router);
    }

    private void unregisterActivity(final NavigationActivity activity) {
        mRouters.remove(activity);
    }

    private void resetAllRouters() {
        for (Router router : mRouters.values()) {
            router.setBackstack(Collections.<RouterTransaction>emptyList(), null);
        }
    }

    private void markReactContextInitialized(NavigationActivity activity, ReactContext reactContext) {
        final NavigatorModule navigator = reactContext.getNativeModule(NavigatorModule.class);
        navigator.setDelegate(activity);

        Log.d(TAG, "RNNL.appDidStarted");
        reactContext.getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class)
                .emit("RNNL.appDidStarted", null);
    }

    private static final String TAG = NavigationProvider.class.getSimpleName();

    private static volatile NavigationProvider sInstance;

    private @Nullable ReactNativeHost mReactHost;
    private @Nullable DoubleTapReloadRecognizer mDoubleTapReloadRecognizer;
    private static final Map<Activity, Router> mRouters = new HashMap<>();
}
