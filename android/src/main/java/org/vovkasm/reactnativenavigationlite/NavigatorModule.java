package org.vovkasm.reactnativenavigationlite;

import android.util.Log;

import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.ReadableMap;
import com.facebook.react.module.annotations.ReactModule;

import org.vovkasm.reactnativenavigationlite.layout.Screen;
import org.vovkasm.reactnativenavigationlite.layout.Stack;

@ReactModule(name="RNNLNavigator")
public class NavigatorModule extends ReactContextBaseJavaModule {
    private static final String TAG = NavigatorModule.class.getSimpleName();

    private Delegate mDelegate = null;

    NavigatorModule(ReactApplicationContext reactContext) {
        super(reactContext);
    }

    @Override
    public String getName() {
        return "RNNLNavigator";
    }

    public void setDelegate(Delegate d) {
        mDelegate = d;
    }

    @ReactMethod
    public void present(String targetId, ReadableMap layout, Promise promise) {
        Log.d(TAG, "present target=" + targetId + " layout=" + layout.toString());
        if (targetId.isEmpty()) {
            if (!layout.getString("type").equals("stack")) {
                throw new RuntimeException("Only stack supported");
            }
            final Stack stack = new Stack(layout);
            if (mDelegate != null) {
                getReactApplicationContext().runOnUiQueueThread(new Runnable() {
                    @Override
                    public void run() {
                        mDelegate.present(stack);
                    }
                });
            }
        } else {
            Log.e(TAG, "concrete target not supported");
        }
        promise.resolve(null);
    }

    @ReactMethod
    public void dismiss(String targetId, Promise promise) {
        Log.d(TAG, "dismiss");
        promise.resolve(null);
    }

    @ReactMethod
    public void push(String targetId, ReadableMap layout, Promise promise) {
        Log.d(TAG, "push target=" + targetId + " layout=" + layout.toString());
        if (!layout.getString("type").equals("screen")) {
            throw new RuntimeException("Only screen can be pushed");
        }
        final Screen screen = new Screen(layout);
        if (mDelegate != null) {
            getReactApplicationContext().runOnUiQueueThread(new Runnable() {
                @Override
                public void run() {
                    mDelegate.push(screen);
                }
            });
        }
        promise.resolve(null);
    }

    @ReactMethod
    public void pop(String targetId, Promise promise) {
        Log.d(TAG, "pop");
        if (mDelegate != null) {
            getReactApplicationContext().runOnUiQueueThread(new Runnable() {
                @Override
                public void run() {
                    mDelegate.pop();
                }
            });
        }
        promise.resolve(null);
    }

    interface Delegate {
        void present(Stack stack);
        void push(Screen screen);
        void pop();
    }
}
