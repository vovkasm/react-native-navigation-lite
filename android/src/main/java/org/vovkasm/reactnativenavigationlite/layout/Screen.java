package org.vovkasm.reactnativenavigationlite.layout;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;

import com.facebook.react.bridge.ReadableMap;

public class Screen implements Parcelable {
    private String id;
    private String componentName;
    private String navigationTitle;

    public Screen(String id, String componentName) {
        this.id = id;
        this.componentName = componentName;
    }

    public Screen(ReadableMap in) {
        id = in.getString("id");
        componentName = in.getString("componentName");
        if (in.hasKey("navigationTitle")) {
            navigationTitle = in.getString("navigationTitle");
        } else {
            navigationTitle = null;
        }
    }

    public String getId() {
        return id;
    }

    public String getComponentName() {
        return componentName;
    }

    public String getNavigationTitle() {
        return navigationTitle;
    }

    public Bundle asArguments() {
        Bundle bundle = new Bundle();
        bundle.putString("id", id);
        bundle.putString("componentName", componentName);
        if (navigationTitle != null) {
            bundle.putString("navigationTitle", navigationTitle);
        }
        return bundle;
    }

    protected Screen(Parcel in) {
        id = in.readString();
        componentName = in.readString();
        Bundle params = in.readBundle();
        navigationTitle = params.getString("navigationTitle");
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(componentName);
        Bundle params = new Bundle();
        if (navigationTitle != null) {
            params.putString("navigationTitle", navigationTitle);
        }
        dest.writeBundle(params);
    }

    public static final Creator<Screen> CREATOR = new Creator<Screen>() {
        @Override
        public Screen createFromParcel(Parcel in) {
            return new Screen(in);
        }

        @Override
        public Screen[] newArray(int size) {
            return new Screen[size];
        }
    };
}
