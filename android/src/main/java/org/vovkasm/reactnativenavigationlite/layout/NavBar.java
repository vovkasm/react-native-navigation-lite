package org.vovkasm.reactnativenavigationlite.layout;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;

import com.facebook.react.bridge.ReadableMap;

public class NavBar implements Parcelable {
    private boolean hidden;

    public NavBar() {
        hidden = false;
    }

    public NavBar(ReadableMap in) {
        if (in.hasKey("hidden")) {
            hidden = in.getBoolean("hidden");
        } else {
            hidden = false;
        }
    }

    public boolean isHidden() {
        return hidden;
    }

    protected NavBar(Parcel in) {
        Bundle params = in.readBundle();
        hidden = params.getBoolean("hidden", false);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        Bundle params = new Bundle();
        params.putBoolean("hidden", hidden);
        dest.writeBundle(params);
    }

    public static final Creator<NavBar> CREATOR = new Creator<NavBar>() {
        @Override
        public NavBar createFromParcel(Parcel in) {
            return new NavBar(in);
        }

        @Override
        public NavBar[] newArray(int size) {
            return new NavBar[size];
        }
    };
}
