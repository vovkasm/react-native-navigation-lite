package org.vovkasm.reactnativenavigationlite.layout;

import android.os.Parcel;
import android.os.Parcelable;

import com.facebook.react.bridge.ReadableArray;
import com.facebook.react.bridge.ReadableMap;

import java.util.ArrayList;
import java.util.List;

public class Stack implements Parcelable {
    private NavBar navBar;
    private ArrayList<Screen> children;

    public Stack(ReadableMap in) {
        if (in.hasKey("navBar")) {
            navBar = new NavBar(in.getMap("navBar"));
        } else {
            navBar = new NavBar();
        }
        if (in.hasKey("children")) {
            ReadableArray arr = in.getArray("children");
            children = new ArrayList<>(arr.size());
            for (int i=0; i<arr.size(); i++) {
                children.add(new Screen(arr.getMap(i)));
            }
        } else {
            throw new RuntimeException("missing children");
        }
    }

    public NavBar getNavBar() {
        return navBar;
    }

    public List<Screen> getChildren() {
        return children;
    }

    protected Stack(Parcel in) {
        navBar = in.readParcelable(NavBar.class.getClassLoader());
        children = new ArrayList<>();
        in.readTypedList(children, Screen.CREATOR);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(navBar, flags);
        dest.writeTypedList(children);
    }

    public static final Creator<Stack> CREATOR = new Creator<Stack>() {
        @Override
        public Stack createFromParcel(Parcel in) {
            return new Stack(in);
        }

        @Override
        public Stack[] newArray(int size) {
            return new Stack[size];
        }
    };
}
