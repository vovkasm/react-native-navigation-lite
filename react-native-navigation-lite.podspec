require 'json'
version = JSON.parse(File.read('package.json'))['version']

Pod::Spec.new do |s|
  s.name            = 'react-native-navigation-lite'
  s.version         = version
  s.homepage        = 'https://github.com/vovkasm/react-native-navigation-lite'
  s.summary         = 'Lite solution for native navigation for react-native'
  s.license         = 'MIT'
  s.author          = { 'Vladimir Timofeev' => 'vovkasm@gmail.com' }
  s.ios.deployment_target = '9.0'
  s.source          = { :git => 'https://github.com/vovkasm/react-native-navigation-lite.git', :tag => "v#{s.version}" }
  s.source_files    = 'ios/RNNavigationLite/*.{h,m}'
  s.preserve_paths  = 'dist'
  s.frameworks      = 'Foundation', 'UIKit'
  s.dependency 'React'
end
