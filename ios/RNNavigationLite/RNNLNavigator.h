#import <Foundation/Foundation.h>

#import <React/RCTBridgeModule.h>
#import <React/RCTEventEmitter.h>

#import "RNNLViewController.h"

@interface RNNLNavigator : RCTEventEmitter <RCTBridgeModule>

- (instancetype)init;

- (void)jsReady;
- (void)screenWillAppear:(NSString*)screenId;
- (void)screenDidAppear:(NSString*)screenId;
- (void)screenWillDisappear:(NSString*)screenId;
- (void)screenDidDisappear:(NSString*)screenId;
- (void)screenDidRemoved:(NSString*)screenId;

@end
