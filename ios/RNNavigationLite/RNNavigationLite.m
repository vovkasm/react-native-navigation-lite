#import "RNNavigationLite.h"

#import <React/RCTBundleURLProvider.h>

#import "RNNLNavigator.h"
#import "RNNLRootViewController.h"
#import "RCTBridge+RNNL.h"

@implementation RNNavigationLite

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    _bridge = [[RCTBridge alloc] initWithDelegate:self launchOptions:launchOptions];
    
    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
    [nc addObserver:self selector:@selector(jsDidLoad) name:RCTJavaScriptDidLoadNotification object:nil];
    [nc addObserver:self selector:@selector(jsWillStartLoading) name:RCTJavaScriptWillStartLoadingNotification object:nil];
    [nc addObserver:self selector:@selector(bridgeWillReload) name:RCTBridgeWillReloadNotification object:nil];
    
    application.delegate.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    application.delegate.window.rootViewController = [[RNNLRootViewController alloc] init];
    [application.delegate.window makeKeyAndVisible];
    return YES;
}

+ (instancetype)sharedInstance {
    static RNNavigationLite* instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[RNNavigationLite alloc] init];
    });
    return instance;
}

#pragma mark - RCTBridgeDelegate

- (NSURL *)sourceURLForBridge:(RCTBridge *)bridge {
    NSURL* bundleURL = [[RCTBundleURLProvider sharedSettings] jsBundleURLForBundleRoot:@"index" fallbackResource:nil];
    return bundleURL;
}

- (NSArray<id<RCTBridgeModule>> *)extraModulesForBridge:(RCTBridge *)bridge {
    RNNLNavigator* navigator = [[RNNLNavigator alloc] init];
    return @[navigator];
}

#pragma mark - Bridge notifications

- (void)jsDidLoad {
    NSLog(@"%s", __func__);
    [self.bridge.rnnlNavigator jsReady];
}

- (void)jsWillStartLoading {
    NSLog(@"%s", __func__);
}

- (void)bridgeWillReload {
    NSLog(@"%s", __func__);
}

@end
