#import "RNNLNavigator.h"

#import "RNNLJSCompletion.h"
#import "RNNLRootViewController.h"
#import "RNNLNavigationController.h"
#import "RNNLViewController.h"

static NSString* const RNNLEventAppDidStarted = @"RNNL.appDidStarted";
static NSString* const RNNLEventScreenDidAppear = @"RNNL.screenDidAppear";
static NSString* const RNNLEventScreenDidDisappear = @"RNNL.screenDidDisappear";
static NSString* const RNNLEventScreenWillAppear = @"RNNL.screenWillAppear";
static NSString* const RNNLEventScreenWillDisappear = @"RNNL.screenWillDisappear";
static NSString* const ENNLEventScreenDidRemoved = @"RNNL.screenDidRemoved";

@interface RNNLNavigator ()

@property (nonatomic, readonly) NSMutableDictionary<NSString*, RNNLViewController*>* controllers;

@property (nonatomic) BOOL isJSReady;

@property (nonatomic) NSInteger observers;

@end

@implementation RNNLNavigator

RCT_EXPORT_MODULE();

- (instancetype)init {
    self = [super init];
    if (self) {
        _controllers = [[NSMutableDictionary alloc] init];
    }
    return self;
}

- (dispatch_queue_t)methodQueue {
    return dispatch_get_main_queue();
}

- (void)jsReady {
    _isJSReady = YES;
    [self maybeSendAppStarted];
}

- (void)maybeSendAppStarted {
    if (self.isJSReady && self.observers > 0) {
        [self sendEventWithName:RNNLEventAppDidStarted body:nil];
    }
}

- (void)screenDidAppear:(NSString *)screenId {
    if (self.observers > 0) {
        [self sendEventWithName:RNNLEventScreenDidAppear body:@{@"screenId": screenId}];
    }
}

- (void)screenDidDisappear:(NSString *)screenId {
    if (self.observers > 0) {
        [self sendEventWithName:RNNLEventScreenDidDisappear body:@{@"screenId": screenId}];
    }
}

- (void)screenWillAppear:(NSString *)screenId {
    if (self.observers > 0) {
        [self sendEventWithName:RNNLEventScreenWillAppear body:@{@"screenId": screenId}];
    }
}

- (void)screenWillDisappear:(NSString *)screenId {
    if (self.observers > 0) {
        [self sendEventWithName:RNNLEventScreenWillDisappear body:@{@"screenId": screenId}];
    }
}

- (void)screenDidRemoved:(NSString *)screenId {
    [self.controllers removeObjectForKey:screenId];
    if (self.observers > 0) {
        [self sendEventWithName:ENNLEventScreenDidRemoved body:@{@"screenId": screenId}];
    }
}

#pragma mark - JavaScript API

RCT_REMAP_METHOD(present,
                 presentFrom:(NSString*)targetId
                 layout:(NSDictionary*)layout
                 resolver:(RCTPromiseResolveBlock)resolve
                 rejecter:(RCTPromiseRejectBlock)reject
                 ) {
    UIViewController* vc = [self buildLayout:layout];
    RNNLJSCompletion* completion = [RNNLJSCompletion completionWithResolver:resolve rejecter:reject];
    if (targetId != nil && targetId.length > 0) {
        UIViewController* targetVC = self.controllers[targetId];
        [targetVC presentViewController:vc animated:YES completion:^{
            completion.resolve(nil);
        }];
    } else {
        // TODO(vovkasm): hack, imho
        RNNLRootViewController* rootVC = (RNNLRootViewController*)[UIApplication sharedApplication].delegate.window.rootViewController;
        [rootVC moveToViewController:vc completion:^{
            completion.resolve(nil);
        }];
    }
}

RCT_REMAP_METHOD(dismiss,
                 dismiss:(NSString*)targetId
                 resolver:(RCTPromiseResolveBlock)resolve
                 rejecter:(RCTPromiseRejectBlock)reject
                 ) {
    UIViewController* targetVC = self.controllers[targetId];
    [targetVC dismissViewControllerAnimated:YES completion:^{
        resolve(nil);
    }];
}

RCT_REMAP_METHOD(push,
                 pushFrom:(NSString*)targetId
                 layout:(NSDictionary*)layout
                 resolver:(RCTPromiseResolveBlock)resolve
                 rejecter:(RCTPromiseRejectBlock)reject
                 ) {
    UIViewController* vc = [self buildLayout:layout];
    UIViewController* targetVC = self.controllers[targetId];
    [targetVC.navigationController pushViewController:vc animated:YES];
    resolve(nil);
}

RCT_REMAP_METHOD(pop,
                 popFrom:(NSString*)targetId
                 withResolver:(RCTPromiseResolveBlock)resolve
                 rejecter:(RCTPromiseRejectBlock)reject
                 ) {
    UIViewController* targetVC = self.controllers[targetId];
    [targetVC.navigationController popViewControllerAnimated:YES];
    resolve(nil);
}

- (NSArray<NSString *> *)supportedEvents {
    return @[RNNLEventAppDidStarted,
             RNNLEventScreenDidAppear,
             RNNLEventScreenDidDisappear,
             RNNLEventScreenWillAppear,
             RNNLEventScreenWillDisappear,
             ENNLEventScreenDidRemoved];
}

- (void)startObserving {
    _observers++;
    [self maybeSendAppStarted];
}

- (void)stopObserving {
    _observers--;
}

#pragma mark - Private

- (UIViewController*)buildLayout:(NSDictionary*)layout {
    if ([layout[@"type"] isEqual:@"screen"]) {
        return [self buildScreenLayout:(NSDictionary*)layout];
    }
    if ([layout[@"type"] isEqual:@"stack"]) {
        return [self buildStackLayout:(NSDictionary*)layout];
    }
    return nil;
}

- (UIViewController*)buildScreenLayout:(NSDictionary*)layout {
    if (layout == nil) return nil;
    if (!layout[@"id"]) return nil;
    NSString* screenId = layout[@"id"];
    if (!layout[@"componentName"]) return nil;
    NSString* name = layout[@"componentName"];
    RNNLViewController* vc = [[RNNLViewController alloc] initWithBridge:self.bridge screenId:screenId name:name];
    if (layout[@"navigationTitle"]) {
        vc.navigationItem.title = layout[@"navigationTitle"];
    }
    self.controllers[screenId] = vc;
    return vc;
}

- (UIViewController*)buildStackLayout:(NSDictionary*)layout {
    RNNLNavigationController* vc = [[RNNLNavigationController alloc] init];
    NSDictionary* navBarOptions = layout[@"navBar"];
    if (navBarOptions) {
        if (navBarOptions[@"hidden"]) {
            [vc setNavigationBarHidden:YES];
        }
    }
    // TODO(vovkasm): fix this!
    vc.navigationBar.translucent = NO;
    NSArray* childLayouts = layout[@"children"];
    if (childLayouts && [childLayouts isKindOfClass:NSArray.class]) {
        NSMutableArray* children = [NSMutableArray array];
        for (NSDictionary* childLayout in childLayouts) {
            UIViewController* child = [self buildLayout:childLayout];
            if (child) {
                [children addObject:child];
            }
        }
        vc.viewControllers = children;
    }
    return vc;
}

@end
