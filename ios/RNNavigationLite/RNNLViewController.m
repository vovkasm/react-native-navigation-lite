#import "RNNLViewController.h"

#import <React/RCTRootView.h>

#import "RCTBridge+RNNL.h"

@interface RNNLViewController ()

@property (nonatomic, weak, readonly) RCTBridge* bridge;
@property (nonatomic) BOOL isDisappeared;
@property (nonatomic) BOOL removeOnDisappear;

@end

@implementation RNNLViewController

- (instancetype)initWithBridge:(RCTBridge *)bridge screenId:(NSString*)screenId name:(NSString *)name {
    self = [self init];
    if (self) {
        _screenId = screenId;
        _name = name;
        _bridge = bridge;
        _isDisappeared = YES;
    }
    return self;
}

- (void)loadView {
    RCTRootView* view = [[RCTRootView alloc] initWithBridge:self.bridge moduleName:self.name initialProperties:@{@"id":self.screenId}];
    view.reactViewController = self;
    self.view = view;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [[UIColor alloc] initWithRed:1.0f green:1.0f blue:1.0f alpha:1.0f];
}

- (void)dismissViewControllerAnimated:(BOOL)flag completion:(void (^)(void))completion {
    UIViewController* presentingVC = self.presentingViewController;
    if (presentingVC) {
        UIViewController* vc = presentingVC.presentedViewController;
        if (vc && [vc conformsToProtocol:@protocol(RNNLViewControllerProtocol)]) {
            [(id <RNNLViewControllerProtocol>)vc rnnlViewControllerRemove];
        }
    }
    [super dismissViewControllerAnimated:flag completion:completion];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    if (self.bridge) {
        [self.bridge.rnnlNavigator screenDidAppear:self.screenId];
    }
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    self.isDisappeared = YES;
    if (self.bridge) {
        [self.bridge.rnnlNavigator screenDidDisappear:self.screenId];
    }
    if (self.removeOnDisappear) {
        [self doRemove];
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.isDisappeared = NO;
    if (self.bridge) {
        [self.bridge.rnnlNavigator screenWillAppear:self.screenId];
    }
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    if (self.bridge) {
        [self.bridge.rnnlNavigator screenWillDisappear:self.screenId];
    }
}

- (void)rnnlViewControllerRemove {
    if (self.isDisappeared) {
        [self doRemove];
    } else {
        self.removeOnDisappear = YES;
    }
}

- (void)doRemove {
    if (self.bridge) {
        [self.bridge.rnnlNavigator screenDidRemoved:self.screenId];
    }
}

@end
