#import "RNNLRootViewController.h"
#import "RNNLViewControllerProtocol.h"

typedef void (^RNNLMoveVCCompletion)(BOOL finished);

@implementation RNNLRootViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
}

- (void)moveToViewController:(UIViewController*)newVC completion:(void(^)(void))completion {
    UIViewController* oldVC = self.contentViewController;
    
    if (oldVC) {
        [oldVC willMoveToParentViewController:nil];
    }
    [self addChildViewController:newVC];
    
    newVC.view.frame = self.view.frame;

    RNNLMoveVCCompletion doneBlock = ^(BOOL finished) {
        self->_contentViewController = newVC;
        if (oldVC) {
            [oldVC removeFromParentViewController];
            if ([oldVC conformsToProtocol:@protocol(RNNLViewControllerProtocol)]) {
                [(id <RNNLViewControllerProtocol>)oldVC rnnlViewControllerRemove];
            }
        }
        [newVC didMoveToParentViewController:self];
        completion();
    };
    
    if (oldVC) {
        newVC.view.alpha = 0.01;
        [self transitionFromViewController:oldVC toViewController:newVC
                                  duration:0.25 options:0
                                animations:^{
                                    newVC.view.alpha = 1.0;
                                    oldVC.view.alpha = 0.01;
                                }
                                completion:doneBlock];
    } else {
        [self.view addSubview:newVC.view];
        doneBlock(YES);
    }
}

@end
