
#import "RNNLNavigationController.h"

@implementation RNNLNavigationController

- (void)rnnlViewControllerRemove {
    for (UIViewController* vc in self.viewControllers) {
        if ([vc conformsToProtocol:@protocol(RNNLViewControllerProtocol)]) {
            [(id <RNNLViewControllerProtocol>)vc rnnlViewControllerRemove];
        }
    }
}

- (UIViewController *)popViewControllerAnimated:(BOOL)animated {
    UIViewController* vc = [super popViewControllerAnimated:animated];
    if (vc && [vc conformsToProtocol:@protocol(RNNLViewControllerProtocol)]) {
        [(id <RNNLViewControllerProtocol>)vc rnnlViewControllerRemove];
    }
    return vc;
}

@end
