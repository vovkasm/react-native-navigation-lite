#import <Foundation/Foundation.h>

#import <React/RCTBridgeModule.h>

@interface RNNLJSCompletion : NSObject

@property (nonatomic, copy, readonly) RCTPromiseResolveBlock resolve;
@property (nonatomic, copy, readonly) RCTPromiseRejectBlock reject;

+ (instancetype)completionWithResolver:(RCTPromiseResolveBlock)resolve rejecter:(RCTPromiseRejectBlock)reject;
- (instancetype)initWithResolver:(RCTPromiseResolveBlock)resolve rejecter:(RCTPromiseRejectBlock)reject;

@end
