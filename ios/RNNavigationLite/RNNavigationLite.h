#import <UIKit/UIKit.h>

#import <React/RCTBridgeDelegate.h>
#import <React/RCTBridge.h>

@interface RNNavigationLite : NSObject <RCTBridgeDelegate>

@property (nonatomic, readonly) RCTBridge* bridge;

+ (instancetype)sharedInstance;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions;

@end
