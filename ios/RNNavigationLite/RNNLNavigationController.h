#import <UIKit/UIKit.h>

#import "RNNLViewControllerProtocol.h"

@interface RNNLNavigationController : UINavigationController <RNNLViewControllerProtocol>

@end
