#import "RCTBridge+RNNL.h"

@implementation RCTBridge (RNNL)

- (RNNLNavigator*)rnnlNavigator {
    return [self moduleForClass:[RNNLNavigator class]];
}

@end
