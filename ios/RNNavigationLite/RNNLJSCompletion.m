#import "RNNLJSCompletion.h"

@implementation RNNLJSCompletion

+ (instancetype)completionWithResolver:(RCTPromiseResolveBlock)resolve rejecter:(RCTPromiseRejectBlock)reject {
    return [[self alloc] initWithResolver:resolve rejecter:reject];
}

- (instancetype)initWithResolver:(RCTPromiseResolveBlock)resolve rejecter:(RCTPromiseRejectBlock)reject {
    self = [super init];
    if (self) {
        _resolve = resolve;
        _reject = reject;
    }
    return self;
}

@end
