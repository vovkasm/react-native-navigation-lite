#pragma once

#import <UIKit/UIKit.h>

@protocol RNNLViewControllerProtocol

- (void)rnnlViewControllerRemove;

@end
