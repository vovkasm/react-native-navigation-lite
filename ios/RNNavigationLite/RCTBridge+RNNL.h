#pragma once

#import <React/RCTBridge.h>
#import "RNNLNavigator.h"

@interface RCTBridge (RNNL)

@property (nonatomic, readonly) RNNLNavigator* rnnlNavigator;

@end
