#import <UIKit/UIKit.h>

@interface RNNLRootViewController : UIViewController

@property (nonatomic, readonly) UIViewController* contentViewController;

- (void)moveToViewController:(UIViewController*)newVC completion:(void(^)(void))completion;

@end
