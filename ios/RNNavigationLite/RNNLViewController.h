#import <UIKit/UIKit.h>

#import <React/RCTBridge.h>

#import "RNNLViewControllerProtocol.h"

@interface RNNLViewController : UIViewController <RNNLViewControllerProtocol>

@property (nonatomic, readonly) NSString* screenId;
@property (nonatomic, readonly) NSString* name;

- (instancetype)initWithBridge:(RCTBridge*)bridge screenId:(NSString*)screenId name:(NSString*)name;

@end
